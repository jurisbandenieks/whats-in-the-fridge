import { useEffect, useState } from "react";
import { IProps } from "../hooks/recipeSubmit";

import {
  Box,
  TextField,
  Switch,
  FormControlLabel,
  Typography,
  Button,
  styled
} from "@mui/material";
import Header from "../components/Header";

import recipeSubmitHandler from "../hooks/recipeSubmit";
import AddFavoriteButton from "../components/AddFavoriteButton";
import { usePrompt } from "../hooks/prompt";

const HomeContainer = styled("div")(({ theme }) => ({
  [theme.breakpoints.down("md")]: {
    width: "90%",
    margin: "0 auto"
  },
  [theme.breakpoints.up("md")]: {
    width: "75%",
    margin: "0 auto"
  },
  [theme.breakpoints.up("lg")]: {
    width: "30%",
    margin: "0 auto"
  }
}));

const Home = () => {
  const { options: promptOptions } = usePrompt();
  const [options, setOptions] = useState<IProps>({
    inventory: "",
    allergies: "",
    weight: 75,
    age: 35,
    isBaby: false
  });

  useEffect(() => {
    setOptions(promptOptions);
  }, [promptOptions]);

  const { submitHandler, recipie, isLoading } = recipeSubmitHandler(options);

  const submitForm = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    submitHandler();
  };

  if (isLoading) return <div>Loading...</div>;

  return (
    <>
      <Header />
      <HomeContainer>
        <Box component="form" onSubmit={submitForm}>
          <TextField
            required
            id="inventory"
            label="Inentory"
            value={options.inventory}
            fullWidth
            margin="dense"
            onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
              setOptions({ ...options, inventory: event.target.value });
            }}
          />

          <TextField
            id="allergies"
            label="Allergies"
            value={options.allergies}
            fullWidth
            margin="dense"
            onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
              setOptions({ ...options, allergies: event.target.value });
            }}
          />

          <TextField
            id="weight"
            type="number"
            label="Weight"
            value={options.weight}
            fullWidth
            margin="dense"
            onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
              setOptions({ ...options, weight: Number(event.target.value) });
            }}
          />

          <TextField
            id="age"
            type="number"
            label="Age"
            value={options.age}
            fullWidth
            margin="dense"
            onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
              setOptions({ ...options, age: Number(event.target.value) });
            }}
          />

          <Box
            sx={{
              display: "flex",
              justifyContent: "center",
              margin: "12px 0"
            }}
          >
            <FormControlLabel
              checked={options.isBaby}
              control={<Switch />}
              label={`Baby: ${options.isBaby}`}
              labelPlacement="start"
              onChange={(event: any) =>
                setOptions({
                  ...options,
                  isBaby: Boolean(event.target.checked)
                })
              }
            />
          </Box>

          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              width: "100%"
            }}
          >
            <Button
              type="submit"
              variant="outlined"
              color="success"
              sx={{
                width: "100%",
                maxWidth: "600px",
                margin: "0 auto"
              }}
            >
              GET RECIPIE
            </Button>
          </Box>
        </Box>

        {!!recipie && (
          <Box sx={{ width: "100%", maxWidth: 500, margin: "32px auto 0" }}>
            <Typography
              variant="body1"
              gutterBottom
              sx={{ whiteSpace: "pre-wrap" }}
            >
              {recipie}
            </Typography>

            <AddFavoriteButton recipie={recipie} />
          </Box>
        )}
      </HomeContainer>
    </>
  );
};

export default Home;
