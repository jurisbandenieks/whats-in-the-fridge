import GoogleLoginButton from "../components/GoogleLoginButton";
import LoginForm from "../components/LoginForm";
import { Typography, Container, Box } from "@mui/material";
import { useAuth } from "../hooks/googlelogin";
import { Navigate } from "react-router-dom";

const Login = () => {
  const { user, loading } = useAuth();

  if (loading) return <div>Loading...</div>;

  return (
    <>
      {!user ? (
        <Container>
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              justifyContent: "center",
              height: "80vh"
            }}
          >
            <Box
              sx={{
                backgroundColor: "#17b978",
                padding: "24px",
                borderRadius: "2px",
                textAlign: "center"
              }}
            >
              <LoginForm />
              <Typography my={2}>- Or Login with -</Typography>
              <GoogleLoginButton />
            </Box>
          </Box>
        </Container>
      ) : (
        <Navigate to="/" replace />
      )}
    </>
  );
};

export default Login;
