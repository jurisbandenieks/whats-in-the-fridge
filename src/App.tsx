import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import { QueryClient, QueryClientProvider } from "react-query";

import Home from "./pages/Home";
import Login from "./pages/Login";
import Favorites from "./pages/Favorites";
import NotFound from "./pages/NotFound";
import Footer from "./components/Footer";
import PrivateRoute from "./components/PrivateRoute";
import { Box } from "@mui/material";

const queryClient = new QueryClient();

const App = () => {
  return (
    <QueryClientProvider client={queryClient}>
      <Router>
        <Box
          sx={{
            marginBottom: "72px"
          }}
        >
          <Routes>
            <Route element={<PrivateRoute />}>
              <Route path="/" element={<Home />} />
              <Route path="/favorites" element={<Favorites />} />
            </Route>
            <Route path="/login" element={<Login />} />

            <Route path="*" element={<NotFound />} />
          </Routes>
        </Box>
        <Footer />
      </Router>
    </QueryClientProvider>
  );
};

export default App;
