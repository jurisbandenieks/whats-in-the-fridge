import { useState } from "react";
import {
  TextField,
  FormControlLabel,
  Box,
  Switch,
  Button
} from "@mui/material";

const LoginForm: React.FC = () => {
  const [email, setEmail] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const [confirmPassword, setConfirmPassword] = useState<string>("");
  const [isNewUser, setIsNewUser] = useState<boolean>(false);

  const submitForm = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
  };

  return (
    <Box
      component="form"
      sx={{
        width: "80%",
        margin: "0 auto"
      }}
      onSubmit={submitForm}
    >
      <TextField
        required
        id="email"
        label="Email"
        type="email"
        defaultValue={email}
        fullWidth
        margin="dense"
        onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
          setEmail(e.target.value);
        }}
      />
      <TextField
        required
        id="password"
        label="Password"
        type="password"
        defaultValue={password}
        fullWidth
        margin="dense"
        onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
          setPassword(e.target.value);
        }}
      />
      {isNewUser && (
        <TextField
          required={isNewUser}
          id="confirmPassword"
          label="Confirm password"
          type="password"
          defaultValue={confirmPassword}
          fullWidth
          margin="dense"
          onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
            setConfirmPassword(e.target.value);
          }}
        />
      )}
      <Box
        sx={{
          display: "flex",
          justifyContent: "center",
          margin: "12px 0"
        }}
      >
        <FormControlLabel
          checked={isNewUser}
          control={<Switch />}
          label="New user?"
          labelPlacement="start"
          onChange={(event: any) => setIsNewUser(Boolean(event.target.checked))}
        />
      </Box>
      <Button
        type="submit"
        variant="outlined"
        sx={{
          width: "100%"
        }}
      >
        {isNewUser ? "Register" : "Login"}
      </Button>
    </Box>
  );
};

export default LoginForm;
