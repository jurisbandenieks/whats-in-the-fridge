import { Logout } from "@mui/icons-material";
import { Button } from "@mui/material";
import { getAuth, signOut } from "firebase/auth";
import { useNavigate } from "react-router-dom";

interface Props {
  children: React.ReactNode;
}

const LogoutButton: React.FC<Props> = ({ children }) => {
  const auth = getAuth();
  const navigate = useNavigate();

  const handleLogout = () => {
    signOut(auth)
      .then(() => {
        navigate("/login");
      })
      .catch((error) => {
        console.error(error);
      });
  };

  return (
    <Button
      variant="contained"
      size="small"
      startIcon={<Logout />}
      onClick={handleLogout}
    >
      {children}
    </Button>
  );
};

export default LogoutButton;
