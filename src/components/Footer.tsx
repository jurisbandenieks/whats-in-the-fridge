import { Box, Typography } from "@mui/material";
import logo from "../assets/openai.png";

const Footer: React.FC = () => {
  return (
    <footer>
      <Box
        sx={{
          position: "fixed",
          bottom: 0,
          left: 0,
          width: "100%",
          backgroundColor: "#086972",
          padding: "24px",
          textAlign: "center",
          display: "flex",
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        <Typography mr={1}> Powered by </Typography>
        <img src={logo} alt="OpenAI" style={{ height: "24px" }} />
      </Box>
    </footer>
  );
};

export default Footer;
