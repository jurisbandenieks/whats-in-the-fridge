import { Button, Box, styled } from "@mui/material";
import { Star } from "@mui/icons-material";
import LogoutButton from "./LogoutButton";
import { Link } from "react-router-dom";

const Header: React.FC = () => {
  const StyledHeader = styled("header")(({ theme }) => ({
    backgroundColor: "#17b978",
    width: "100%",
    padding: "24px",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    margin: "0 0 24px",

    [theme.breakpoints.down("md")]: {
      flexDirection: "column"
    },
    [theme.breakpoints.up("lg")]: {
      flexDirection: "row"
    }
  }));

  const StyledHeadline = styled("h1")(({ theme }) => ({
    fontSize: "24px",
    margin: "0",
    padding: "0",

    [theme.breakpoints.down("md")]: {
      fontSize: "16px",
      margin: "0 0 12px 0"
    },

    a: {
      textDecoration: "none",
      color: "#086972"
    }
  }));

  return (
    <StyledHeader>
      <StyledHeadline>
        <Link to="/">What's in the fridge?</Link>
      </StyledHeadline>
      <Box
        sx={{
          display: "flex",
          justifyContent: "flex-end"
        }}
      >
        <Link to="/favorites">
          <Button
            color="success"
            variant="contained"
            size="small"
            startIcon={<Star />}
            sx={{
              margin: "0 16px 0 0"
            }}
          >
            Favorites
          </Button>
        </Link>
        <LogoutButton>Logout</LogoutButton>
      </Box>
    </StyledHeader>
  );
};

export default Header;
