import { Star } from "@mui/icons-material";
import { Button } from "@mui/material";

interface Props {
  recipie: string;
}

const AddFavoriteButton: React.FC<Props> = ({ recipie }) => {
  const handleSave = () => {
    console.log("Save recipe =>", recipie);
  };

  return (
    <Button
      variant="outlined"
      startIcon={<Star />}
      sx={{
        width: "100%",
        maxWidth: "600px",
        margin: "24px auto"
      }}
      onClick={handleSave}
    >
      Add Favorite
    </Button>
  );
};

export default AddFavoriteButton;
