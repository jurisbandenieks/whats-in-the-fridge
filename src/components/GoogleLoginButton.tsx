import GoogleButton from "react-google-button";
import { Box } from "@mui/material";
import {
  GoogleAuthProvider,
  UserCredential,
  signInWithPopup
} from "firebase/auth";

import { auth } from "../firebase";
import { useNavigate } from "react-router-dom";

const GoogleLoginButton: React.FC = () => {
  const provider = new GoogleAuthProvider();
  const navigate = useNavigate();

  const googleLoginHandler = async () => {
    try {
      const userCred: UserCredential | null = await signInWithPopup(
        auth,
        provider
      );

      if (userCred) {
        navigate("/");
      }
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <Box
      sx={{
        display: "flex",
        justifyContent: "stretch"
      }}
    >
      <GoogleButton
        className="login-google"
        type="dark"
        onClick={googleLoginHandler}
      />
    </Box>
  );
};

export default GoogleLoginButton;
