import { useEffect, useState } from "react";
import { IProps as OptionProps } from "./recipeSubmit";
import {
  DocumentData,
  QuerySnapshot,
  collection,
  getDocs,
  query,
  where
} from "firebase/firestore";
import { db } from "../firebase";
import { useAuth } from "./googlelogin";
import { differenceInYears } from "date-fns";

export const usePrompt = () => {
  const [options, setOptions] = useState<OptionProps>({
    inventory: "",
    allergies: "",
    weight: 75,
    age: 35,
    isBaby: false
  });
  const [isLoading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<Error | null>(null);

  const { user } = useAuth();

  useEffect(() => {
    async function fetchUserData() {
      try {
        if (!!user?.email) {
          setLoading(true);
          const userRef = collection(db, "Users");
          const q = query(userRef, where("email", "==", user.email));

          const querySnapshot: QuerySnapshot = await getDocs(q);
          querySnapshot.forEach((doc: DocumentData): void => {
            if (doc.data()) {
              const data = doc.data();
              const birthDate = new Date(data.age.seconds * 1000); // replace with your birth date
              const today = new Date(); // replace with the current date

              const ageInYears = differenceInYears(today, birthDate);

              setOptions({
                age: ageInYears,
                weight: data.weight,
                isBaby: data.isBaby ?? false,
                allergies: data.allergies.join(", "),
                inventory: data.inventory.join(", ")
              });
            }
          });

          setLoading(false);
        }
      } catch (error: any) {
        setError(error);
      }
    }

    fetchUserData();
  }, [user]);

  return { options, isLoading, error };
};
