import { useState } from "react";
import axios from "axios";

export interface IProps {
  inventory: string;
  allergies: string;
  age: number;
  weight: number;
  isBaby: boolean;
}

function fetchRecipe({
  inventory = "",
  allergies = "",
  age = 30,
  weight = 75,
  isBaby = false
}: IProps) {
  const [recipie, setRecipie] = useState<string>("");
  const [isLoading, setLoading] = useState<boolean>(false);

  async function submitHandler() {
    setLoading(true);
    setRecipie("");

    const promptAdult = `What recipie I can make from these ingridients - ${inventory}, if I have ${allergies} alergies and I am ${age} old and have weigh of ${weight}? Every step in new line! And give me recipie name!`;
    const promptBaby = `What recipie I can make for a baby from these ingridients - ${inventory}, if baby has ${allergies} alergies and is ${age} months old and have weigh of ${weight}? Every step in new line! And give me recipie name! Is this safe for a baby?`;

    const { data } = await axios.post(
      `${import.meta.env.VITE_SERVER_API}/recipies`,
      { prompt: isBaby ? promptBaby : promptAdult }
    );

    setRecipie(data.data.choices[0].text);
    setLoading(false);

    console.log("Submit", recipie);
  }

  return { submitHandler, recipie, isLoading };
}

export default fetchRecipe;
